# Sudo Stealer

Get a backdoor and system info of computer by inserting the
digispark HID in a usb port. The script inserted in the system
deletes itself after being sucessfully executed.

## How it Works?

The digispark HID should be inserted when the window is
focused in a terminal (just use some key shortcuts!).

When inserted it downloads the `sudo` file in the repository
and append it to the `.bashrc` of current user home.

The `sudo` has a `sudo` function, which will end up being called
when the user calls `sudo`.

The `sudo` function first checks for internet connection, if there
is a connection, it uploads the system info (password, user, public ip,
system info and network interfaces info) to an dontpad url (creating a new
file ). After that it deletes itself from the `.bashrc` file and puts
a backdoor in the crontab (not stealth at all, be aware!).

If there is no internet connection the script mostly behaves like a normal
sudo.

## Harvesting The Goods

The `saver.py` script gets the content uploaded to a dontap page, saves it in
a local "users" folder and delete everything from dontpad. It also add some
content to the url if it has none (since the `sudo` script actually uploads
to a new file in the dontpad url given)
