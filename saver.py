#!/usr/bin/env python3
import requests
import os
from time import sleep

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import TimeoutException

error = "Check connection and try again!"

# yes, the url is a long boi
url = "http://dontpad.com/45d28e64c96c8c53bc8de52f94a974edd32392a3de85238d48029f260c67970e5b0ef6517e1352aa65a8ff896ec06111536f80708ab49a55feb8f4f11809e176"

def delete_subfolder(driver, subfolder_name, timeout=10, wait_update=3):

    driver.get(url + "/" + subfolder_name)

    try:
        textarea = WebDriverWait(driver, timeout).until(
                    EC.element_to_be_clickable((By.XPATH,"//textarea[@id=\"text\"]"))
                )
    except TimeoutException:
        print("couldn't get", subfolder_name, "!", error)
        driver.quit()
        quit()

    textarea.send_keys(Keys.ARROW_RIGHT)
    textarea.send_keys(Keys.BACKSPACE)

    driver.get(url)

    sleep(wait_update)

# make sure there is text in the home url (can't post to subfolders if it doesn't)
if( requests.get(url + ".txt").text == ""):

    print("nothing on main page. going to fill it with something.")

    # fill page with the chinese anthem if there is nothing
    # (nothing political, just the first thing that i though of googling)
    if( requests.post(url, data={'text': '狼人惡作劇'}).status_code == 200):

        print("page was filled sucessfully")
    else:

        print("couldn't post text to main page!", error)
        quit()

# get list of users
menu = requests.get(url + ".menu.json")

if( menu.status_code == 200 ):

    print("list of subfolders sucessfully acquired")
else:
    print("couldn't get list of subfolders!", error)
    quit()

menu = menu.text[2:-2].split("\",\"")

if( menu == [''] ):

    print("Nothing new to save! No changes made!")

else:

    print("new users: ")
    for user in menu: print('\t' + user)

    # check if we have a directory to save stuff
    if( not os.path.isdir("users") ):

        if( os.path.exists("users") ):

            print("Couldn't create directory 'users': a file in the current directory already has this name!")
            print("move or delete it and run the program again.")
            quit()

        else:

            os.mkdir("users")
            print("created 'users' directory, your findings will be saved there.")

    # prepare selenium webdriver, to delete subfolders
    options = Options()
    options.headless = True
    driver = webdriver.Firefox(options=options)

    # go to each user in the menu
    for user in menu:

        # get the user's page html
        html = requests.get( url + "/" + user + ".txt" )

        if( html.status_code != 200 ):

            print("couldn't get user", user, " page!", error)
            driver.quit()
            quit()

        with open("users/" + user, "w") as f:

            if( html.text != ' ' ): f.write(html.text)
            print(user, "info saved! Preparing to delete info in dontpad")
        
        # now delete the evidence so the cops don't arrest us (yes i said US)
        # you're stuck with me in this shit now. It could have been different
        # but you just had to kill coby right? Why did you even do that?
        # now we are stuck trying to make a living out of this shit
        # god damnit rodrick, every fucking time
        # i was just enjoying my life and now i am stuck in this mess
        # that YOU made up

        # obs: it's late, i should sleep
        # obs2: thats why i write code, not suspense-driven books
        # obs3: ok, back to work. Let's delete the evidence

        if( requests.post(url + "/" + user, data={'text':' '}).status_code != 200 ):

            print("couldn't delete user", user, "subfolder", error)
            driver.quit()
            quit()

        else:

            print("deleting subfolder...")

            # delete subfolder
            delete_subfolder(driver, user)

            # check if user was actually deleted
            with requests.get(url + "/" + user + ".txt") as result:

                if( result.status_code != 200 ):

                    print("couldn't check if subfolder was actually deleted!", error)
                    driver.quit()
                    quit()

                elif( result.text == "" ):

                    print("subfolder", user, "sucessfully erased!")

                else:
                    print("this subfolder ("+ user + ") still contains content! This shouldn't have happened. Check the content and run the program again")

    # put driver away
    driver.quit()
